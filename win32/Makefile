#	$Id: Makefile,v 2.16 1999/11/09 18:55:03 crosser Exp $
#
#	Copyright (c) 1997,1998 Eugene G. Crosser
#	Copyright (c) 1998 Bruce D. Lightner (DOS/Windows support)
#
#	You may distribute and/or use for any purpose modified or unmodified
#	copies of this software if you preserve the copyright notice above.
#
#	THIS SOFTWARE IS PROVIDED AS IS AND COME WITH NO WARRANTY OF ANY
#	KIND, EITHER EXPRESSED OR IMPLIED.  IN NO EVENT WILL THE
#	COPYRIGHT HOLDER BE LIABLE FOR ANY DAMAGES RESULTING FROM THE
#	USE OF THIS SOFTWARE.
#
# 	sample message
#	$Log: Makefile,v $
#	Revision 2.16  1999/11/09 18:55:03  crosser
#	work on windows version
#	
#	Revision 2.15  1999/08/04 11:52:21  ady
#	test commit
#	
#	Revision 2.14  1999/08/01 23:25:11  crosser
#	move makefile to subdirectory, NOT working yet!
#	
#	Revision 2.13  1999/04/14 18:34:38  lightner
#	link with setargv to expand filenames
#
#	Revision 2.12  1999/03/22 06:08:44  lightner
#	Add -DHAVE_RENAME
#	Fix overly long command line problem (add $(EXTRA_OBJS))
#	Fix "make clean" to do full cleanup
#
#	Revision 2.11  1999/03/12 23:22:30  crosser
#	define for new utime() code
#
#	Revision 2.10  1999/03/12 10:06:23  crosser
#	setfiletime
#
#	Revision 2.9  1999/03/10 22:34:43  crosser
#	sync with structure changes
#
#	Revision 2.8  1999/01/17 13:37:29  crosser
#	fix copyright info
#
#	Revision 2.7  1999/01/17 13:27:59  bruce
#	make win/dos versions in separate subdirs
#
#	Revision 2.6  1998/10/18 13:18:27  crosser
#	Put RCS logs and I.D. into the source
#
#	Revision 2.5  1998/02/16 06:12:02  lightner
#	Add dependancy for revision.h
#	
#	Revision 2.4  1998/02/08 19:58:38  crosser
#	Support low memory: chunked saving etc.
#	
#	Revision 2.3  1998/02/05 23:33:00  lightner
#	Fix make logic to allow debug
#	Fix "make clean" logic to remove MSVC++ "trash/debug" files
#	
#	Revision 2.2  1998/01/18 02:16:45  crosser
#	DOS support
#	HAVE_NANOSLEEP
#	
#	Revision 2.1  1998/01/03 12:50:02  crosser
#	fix for new name etc.
#	
#	Revision 2.0  1998/01/02 19:20:11  crosser
#	Added support for Win32
#	
#	Revision 1.1  1998/01/02 00:49:34  crosser
#	Initial revision
#
# Modified by Bruce D. Lightner (lightner@lightner.net) on 1/2/98 to
# support builds under Microsoft Windows 95/NT
#
# To "make" using Microsoft's Visual C++ toolset, ensure that your
# PATH points to the proper development tools directory, verify that
# the INCLUDE and LIB environment variables are set, then enter the
# following commands:
#
#      nmake clean
#      nmake

srcdir	= ..
VPATH	= ..

OPTS	= -DHAVE_CONFIG_H

CC	= cl
#CFLAGS	= -O2 -Zi
CFLAGS	= -O2
LD	= cl
LDFLAGS	= $(CFLAGS)
AR	= lib

# "-I." go first to include right config.h !
INC = -I. -I$(srcdir)

##### No need to touch anything below this line #####

LIBOBJS = eph_read.obj eph_iob.obj eph_err.obj \
	eph_cmd.obj eph_io.obj eph_open.obj

EXTRA_OBJS = getopt.obj usleep.obj strcscmp.obj \
	a12scan.obj fnames.obj ctimez.obj filetime.obj \
	exifscan.obj

all:	eph_io.lib photopc.exe epinfo.exe

eph_io.lib:      $(LIBOBJS)
	$(AR) /out:$@ $(LIBOBJS)

photopc.exe:     photopc.obj $(EXTRA_OBJS) eph_io.lib
	$(LD) $(LDFLAGS) -Fe$@ photopc.obj $(EXTRA_OBJS) eph_io.lib

epinfo.exe:      epinfo.obj $(EXTRA_OBJS)
	$(LD) $(LDFLAGS) -Fe$@ epinfo.obj $(EXTRA_OBJS) /link setargv.obj


tidyup:
	del *.obj
	del vc*.pdb
	del eph_io.bak
	del photopc.vcp
	del photopc.ilk
	del photopc.pdb
	del epinfo.vcp
	del epinfo.ilk
	del epinfo.pdb

clean:	tidyup
	del eph_io.lib
	del photopc.exe
	del epinfo.exe

MKOBJ =	$(CPP) $(CFLAGS) $(OPTS) $(INC) -Fo$@ -c

photopc.obj: $(srcdir)/photopc.c $(srcdir)/eph_io.h $(srcdir)/a12scan.h $(srcdir)/fnames.h $(srcdir)/ctimez.h $(srcdir)/filetime.h $(srcdir)/getopt.h $(srcdir)/strcscmp.h
	$(MKOBJ) -ophotopc.obj $(srcdir)/photopc.c

epinfo.obj: $(srcdir)/epinfo.c $(srcdir)/a12scan.h $(srcdir)/exifscan.h $(srcdir)/fnames.h $(srcdir)/ctimez.h $(srcdir)/filetime.h $(srcdir)/getopt.h $(srcdir)/strcscmp.h
	$(MKOBJ) -oepinfo.obj $(srcdir)/epinfo.c

a12scan.obj: $(srcdir)/a12scan.c $(srcdir)/a12scan.h
	$(MKOBJ) -oa12scan.obj $(srcdir)/a12scan.c

exifscan.obj: $(srcdir)/exifscan.c $(srcdir)/exifscan.h
	$(MKOBJ) -oexifscan.obj $(srcdir)/exifscan.c

fnames.obj: $(srcdir)/fnames.c $(srcdir)/fnames.h $(srcdir)/ctimez.h
	$(MKOBJ) -ofnames.obj $(srcdir)/fnames.c

ctimez.obj: $(srcdir)/ctimez.c $(srcdir)/ctimez.h
	$(MKOBJ) -octimez.obj $(srcdir)/ctimez.c

filetime.obj: $(srcdir)/filetime.c $(srcdir)/filetime.h $(srcdir)/ctimez.h
	$(MKOBJ) -ofiletime.obj $(srcdir)/filetime.c

eph_read.obj: $(srcdir)/eph_read.c $(srcdir)/eph_io.h $(srcdir)/comio.h $(srcdir)/usleep.h
	$(MKOBJ) -oeph_read.obj $(srcdir)/eph_read.c

eph_iob.obj: $(srcdir)/eph_iob.c $(srcdir)/eph_io.h
	$(MKOBJ) -oeph_iob.obj $(srcdir)/eph_iob.c

eph_err.obj: $(srcdir)/eph_err.c $(srcdir)/eph_io.h $(srcdir)/eph_priv.h
	$(MKOBJ) -oeph_err.obj $(srcdir)/eph_err.c

eph_cmd.obj: $(srcdir)/eph_cmd.c $(srcdir)/eph_io.h $(srcdir)/eph_priv.h
	$(MKOBJ) -oeph_cmd.obj $(srcdir)/eph_cmd.c

eph_io.obj: $(srcdir)/eph_io.c $(srcdir)/eph_io.h $(srcdir)/eph_priv.h $(srcdir)/usleep.h $(srcdir)/comio.h
	$(MKOBJ) -oeph_io.obj $(srcdir)/eph_io.c

eph_open.obj: $(srcdir)/eph_open.c $(srcdir)/eph_io.h $(srcdir)/eph_priv.h $(srcdir)/usleep.h $(srcdir)/comio.h $(srcdir)/usleep.h
	$(MKOBJ) -oeph_open.obj $(srcdir)/eph_open.c

getopt.obj: $(srcdir)/getopt.c $(srcdir)/getopt.h
	$(MKOBJ) -ogetopt.obj $(srcdir)/getopt.c

usleep.obj: $(srcdir)/usleep.c
	$(MKOBJ) -ousleep.obj $(srcdir)/usleep.c

strcscmp.obj: $(srcdir)/strcscmp.c
	$(MKOBJ) -ostrcscmp.obj $(srcdir)/strcscmp.c

