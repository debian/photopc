photopc (3.07-4) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to describe branch layout.
  * Updated vcs in d/control to Salsa.
  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.5.0 to 4.7.0.
  * Use wrap-and-sort -at for debian control files
  * Drop unnecessary dependency on dh-autoreconf.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 19 May 2024 06:43:56 +0200

photopc (3.07-3) unstable; urgency=medium

  * QA upload.
  * Fix building with -Werror=implicit-function-declaration.
    (Closes: #1066511)
  * Declare Rules-Requires-Root: no.

 -- Andreas Beckmann <anbe@debian.org>  Fri, 05 Apr 2024 03:45:07 +0200

photopc (3.07-2) unstable; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelp-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Bumped Standards Version to 4.5.0.

 -- Jaitony de Sousa <jaitonys@gmail.com>  Sun, 17 May 2020 12:44:58 -0300

photopc (3.07-1) unstable; urgency=medium

  * QA upload.
  * New upstream release (LP: #906524).
  * Migrations:
      - debian/copyright to 1.0 format.
      - Bumped Standards-Version to 3.9.6.
  * debian/control:
      - Included Homepage field.
      - Removed outdated information of long-description.
      - Removed extra spaces.
      - Removed automake1.11 from Build-Depends field.
  * debian/copyright:
      - Migrated to 1.0 format.
      - Revised and updated all information.
  * debian/patches:
      - Included patch to fix de-ANSI-fication error.
      - Included patch to fix spelling errors in manpages.
      - Included patch to fix spelling errors in photopc.c file.
  * debian/photopc.docs:
      - Removed useless doc about DOS.
  * debian/README.Debian:
      - Included with information about unofficial protocol.
  * debian/rules:
      - Included DEB_BUILD_MAINT_OPTIONS for hardening.
  * debian/watch: Included.

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Tue, 03 Nov 2015 11:23:00 -0200

photopc (3.05-8) unstable; urgency=medium

  * QA upload.
  * Switch to "3.0 (quilt)" source format.
  * Add debian/patches/01-the-build-date-should-not-matter.

 -- Santiago Vila <sanvila@debian.org>  Sat, 29 Aug 2015 16:46:52 +0200

photopc (3.05-7) unstable; urgency=low

  * QA upload.
  * Maintainer field set to QA Group.
  * Bump Standards-Version to 3.9.5.
  * Set debhelper compatibility level to 9.
  * Rewrite debian/rules using dh --with autoreconf.
  * Depend on ${misc:Depends}.
  * Explicitly list documentation files in debian/photopc.docs.

 -- Emanuele Rocca <ema@debian.org>  Mon, 24 Mar 2014 17:11:41 +0100

photopc (3.05-6) unstable; urgency=low

  * New Maintainer (Closes #261681)
  * Updated debhelper compatibility version to 4
  * Updated Standards-Version to current policy
  * Fixed disparities found in Override and .deb, changed to graphic - extra
  * Modified debian/rules thanks to the contribution of David Moreno Garza <damog@debian.org>

 -- Ana Isabel Delgado Dominguez <anubis@debianvenezuela.org>  Fri, 28 Oct 2005 21:43:50 -0400

photopc (3.05-5) unstable; urgency=low

  * Switched from debmake to debhelper and do some cleanup.

 -- Santiago Vila <sanvila@debian.org>  Thu,  6 Jan 2005 18:16:20 +0100

photopc (3.05-4) unstable; urgency=low

  * Orphaning this package.

 -- John Goerzen <jgoerzen@complete.org>  Tue, 27 Jul 2004 09:27:04 -0500

photopc (3.05-3) unstable; urgency=low

  * Turn off debstd installation of manpages; use dh_installman now.
    Closes: #215931.
  * Aggresified clean target to also remove some config status files.

 -- John Goerzen <jgoerzen@complete.org>  Tue, 21 Oct 2003 22:37:44 -0500

photopc (3.05-2) unstable; urgency=low

  * Build-depends on debmake.  Closes: #190502.

 -- John Goerzen <jgoerzen@complete.org>  Mon, 18 Aug 2003 17:54:10 -0500

photopc (3.05-1) unstable; urgency=low

  * New upstream release.  Closes: #84811.
  * Corrected description typo.  Closes: #125235.

 -- John Goerzen <jgoerzen@complete.org>  Sat, 20 Apr 2002 12:26:43 -0500

photopc (3.04-1) unstable; urgency=low

  * New upstream release.  Closes: #69095.

 -- John Goerzen <jgoerzen@complete.org>  Mon,  4 Sep 2000 12:34:52 -0500

photopc (3.02-2) unstable; urgency=low

  * Remove ansi2knr manpage that debstd erroneously includes.

 -- John Goerzen <jgoerzen@complete.org>  Sun, 19 Dec 1999 17:58:30 -0600

photopc (3.02-1) unstable; urgency=low

  * New upstream release.  Closes: #52399, #46011.

 -- John Goerzen <jgoerzen@complete.org>  Sat, 18 Dec 1999 16:25:03 -0600

photopc (3.01-1) unstable; urgency=low

  * New upstream release
  * Deleted old README.debian file.
  * Updated copyright file with updated notice from README.
  * Updated standards-version

 -- John Goerzen <jgoerzen@complete.org>  Wed,  1 Sep 1999 20:10:07 -0500

photopc (2.8-3) unstable; urgency=low

  * Now installs .h and .a files.

 -- John Goerzen <jgoerzen@complete.org>  Fri, 11 Dec 1998 13:57:14 -0600

photopc (2.8-2) unstable; urgency=low

  * Fixed Maintainer line in control file.

 -- John Goerzen <jgoerzen@complete.org>  Wed,  9 Dec 1998 22:58:29 -0600

photopc (2.8-1) unstable; urgency=low

  * New upstream release, fixes some problems with download errors
    or timeouts since the 2.1 release.  Fixes bug #30544
  * New maintainer.

 -- John Goerzen <jgoerzen@complete.org>  Wed,  9 Dec 1998 18:13:35 -0600

photopc (2.1-1) unstable; urgency=low

  * Initial Release.

 -- Christoph Lameter <chris@lameter.com>  Wed, 18 Feb 1998 17:08:55 -0800
