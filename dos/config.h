#include "version.h"

/* Define if you have the strftime function.  */
#define HAVE_STRFTIME 1

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* Integer 16bit type */
#define INT16 short

/* Integer 3bit type */
#define INT32 long

/* Platform definition */
#define DOS 1

/* Low memory model */
#define LOWMEMORY 1

/* The number of bytes in a int.  */
#define SIZEOF_INT 4

/* The number of bytes in a long.  */
#define SIZEOF_LONG 4

/* The number of bytes in a short.  */
#define SIZEOF_SHORT 2

/* Define if you have the getopt function.  */
#define HAVE_GETOPT 1

/* Define if you have the mkdir function.  */
#define HAVE_MKDIR 1

/* Define if you have the rename function.  */
#define HAVE_RENAME 1

/* Define if you have the strerror function.  */
#define HAVE_STRERROR 1

/* Define if you have the strspn function.  */
#define HAVE_STRSPN 1

/* Define if you have the <fcntl.h> header file.  */
#define HAVE_FCNTL_H 1

/* Define if you have the <string.h> header file.  */
#define HAVE_STRING_H

/* Define if you have the <sys/select.h> header file.  */
#define HAVE_SYS_SELECT_H 1

/* Define if you have the <utime.h> header file.  */
#define HAVE_UTIME_H 1

/* Define if compiler has function prototypes */
#define PROTOTYPES 1

/* is struct utimbuf defined in sys/utime.h */
#define HAVE_UTIMBUF 1

