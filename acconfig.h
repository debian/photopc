/*
	$Id: acconfig.h,v 1.9 2000/07/14 06:35:39 crosser Exp $
*/

/*
	Copyright (c) 1997,1998 Eugene G. Crosser
	Copyright (c) 1998 Bruce D. Lightner (DOS/Windows support)

	You may distribute and/or use for any purpose modified or unmodified
	copies of this software if you preserve the copyright notice above.

	THIS SOFTWARE IS PROVIDED AS IS AND COME WITH NO WARRANTY OF ANY
	KIND, EITHER EXPRESSED OR IMPLIED.  IN NO EVENT WILL THE
	COPYRIGHT HOLDER BE LIABLE FOR ANY DAMAGES RESULTING FROM THE
	USE OF THIS SOFTWARE.
*/

/*
	$Log: acconfig.h,v $
	Revision 1.9  2000/07/14 06:35:39  crosser
	reportedly some models return CAN instead of DC1
	
	Revision 1.8  2000/05/09 13:20:54  crosser
	configure read() with alarm() better.
	Address signed vs. unsigned arguments
	other cleanups to make most notorious compilers happy
	
	Revision 1.7  2000/02/04 22:31:56  crosser
	Clean up things dealing with folder tree recursion.
	Fixed log entry in acconfig.h that conflicted with "configure"
	
	Revision 1.6  2000/02/01 22:04:15  crosser
	xchdir() function iterates thru path elements
	define a variable for extra verbosity
	
	Revision 1.5  1999/12/11 14:10:15  crosser
	Support sgtty terminal control
	Proper "fake speed" handling (needed two values)
	
	Revision 1.4  1999/11/17 13:48:08  crosser
	check if need to include getopt.h
	
	Revision 1.3  1999/11/09 18:55:03  crosser
	work on windows version
	
	Revision 1.2  1999/08/01 23:28:06  crosser
	add RTPRIO

	Revision 1.1  1999/07/29 06:48:17  crosser
	Initial revision

*/

/* Integer 16bit type */
#undef INT16

/* Integer 3bit type */
#undef INT32

/* Platform definition */
#undef UNIX
#undef MSWINDOS
#undef DOS

/* Low memory model */
#undef LOWMEMORY

/* Realtime priority */
#undef USE_RTPRIO

/* struct tm has tm_gmtoff member */
#undef HAVE_TM_GMTOFF

/* headers contain definition of optarg */
#undef OPTARG_DEFINED

/* terminal control type */
#undef USE_TERMIOS
#undef USE_SGTTY
#undef USE_TERMIO

/* is struct utimbuf defined in sys/utime.h */
#undef HAVE_UTIMBUF

/* use blocking read() in conjunction with alarm() instead of select() */ 
#undef USE_ALARMED_READ

/* devine for development versions - extra verbosity */
#undef DEVVERSION

/* default device name */
#undef DEFAULT_DEVICE
